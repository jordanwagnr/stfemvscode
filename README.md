# VS Code Configuration for C++
This repository include files to configure vscode for editing the stfem code in c++.
It uses the EMACS improved package along with custom keyboard shortcuts to enable
almost full functionality of EMACS cua-mode bindings. There are several other extensions
included that I use all the time. Also, there are several snippets that include commenting
macros for doxygen and others.

- Copy the contents of /extensions into your local ~/.vscode/extensions directory
- Copy the contents of /User into your local ~./.config/Code/User directory
- Copy the contents of Workspace into the .vscode folder into your /stfem project directory