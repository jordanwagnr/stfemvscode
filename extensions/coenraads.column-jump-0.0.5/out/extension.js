'use strict';
Object.defineProperty(exports, "__esModule", { value: true });
const vscode = require("vscode");
const vscode_1 = require("vscode");
function activate(context) {
    context.subscriptions.push(vscode.commands.registerCommand('column-jump.jumpDown', () => {
        const editor = vscode.window.activeTextEditor;
        if (!editor) {
            return;
        }
        editor.selections = editor.selections
            .map(sel => jumpDown(sel, editor))
            .filter(sel => !!sel);
        scrollFirstSelectionIntoView(editor);
    }));
    context.subscriptions.push(vscode.commands.registerCommand('column-jump.jumpUp', () => {
        const editor = vscode.window.activeTextEditor;
        if (!editor) {
            return;
        }
        editor.selections = editor.selections
            .map(sel => jumpUp(sel, editor))
            .filter(sel => !!sel);
        scrollFirstSelectionIntoView(editor);
    }));
}
exports.activate = activate;
function jumpUp(selection, editor) {
    const cursorPos = selection.active, char = cursorPos.character;
    for (let i = cursorPos.line - 1; i >= 0; i--) {
        if (isLineBlocking(editor.document.lineAt(i), char)) {
            return makeSelection(i, char);
        }
    }
    return undefined;
}
function jumpDown(selection, editor) {
    const cursorPos = selection.active, lineCount = editor.document.lineCount, char = cursorPos.character;
    for (let i = cursorPos.line + 1; i < lineCount; i++) {
        if (isLineBlocking(editor.document.lineAt(i), char)) {
            return makeSelection(i, char);
        }
    }
    return undefined;
}
function isLineBlocking(line, char) {
    return (!line.isEmptyOrWhitespace &&
        line.range.end.character >= char &&
        line.firstNonWhitespaceCharacterIndex <= char);
}
function makeSelection(line, char) {
    const newPos = new vscode_1.Position(line, char);
    return new vscode_1.Selection(newPos, newPos);
}
function scrollFirstSelectionIntoView(editor) {
    if (editor.selections.length < 1) {
        return;
    }
    const pos = editor.selections[0].active;
    editor.revealRange(new vscode_1.Range(pos, pos));
}
function deactivate() {
}
exports.deactivate = deactivate;
//# sourceMappingURL=extension.js.map